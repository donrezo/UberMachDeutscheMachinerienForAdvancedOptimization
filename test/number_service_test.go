package service

import (
	"UberMachDeutscheMachinerienForAdvancedOptimization/internal/adapters/logger"
	"UberMachDeutscheMachinerienForAdvancedOptimization/internal/core/service"
	"os"
	"testing"
)

func TestFindNumberIndex(t *testing.T) {
	// Create a slice of numbers that will be used for testing
	numbers := []int{0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000}

	logger := logger.NewZerologAdapter("debug")
	numberService := service.NewNumberService(numbers, logger)

	// Define test cases
	testCases := []struct {
		value         int
		expectedIndex int
		expectError   bool
	}{
		{100, 1, false},   // A number that exists
		{900, 9, false},   // Another number that exists
		{0, 0, false},     // Boundary condition: first element
		{1000, 10, false}, // Boundary condition: last element
		{150, -1, true},   // A number that does not exist
		{99999, -1, true}, // A number outside the range
	}

	for _, tc := range testCases {
		index, _, err := numberService.FindNumberIndex(tc.value)

		// Check if the result met the expectation
		if tc.expectError {
			if err == nil {
				t.Errorf("Expected an error for value %d, but got none", tc.value)
			}
		} else {
			if err != nil {
				t.Errorf("Did not expect an error for value %d, but got one: %v", tc.value, err)
			}
			if index != tc.expectedIndex {
				t.Errorf("Expected index %d for value %d, got %d", tc.expectedIndex, tc.value, index)
			}
		}
	}
}

func TestLoadNumbers(t *testing.T) {
	// Create a temporary file with test data
	tmpfile, err := os.CreateTemp("", "testnumbers")
	if err != nil {
		t.Fatalf("Could not create temp file: %v", err)
	}
	defer os.Remove(tmpfile.Name()) // clean up

	// Write test data to the file
	_, err = tmpfile.WriteString("0\n100\n200\nabc\n300\n")
	if err != nil {
		t.Fatalf("Could not write to temp file: %v", err)
	}
	// Make sure to close the file so that the scanner can open it later
	if err := tmpfile.Close(); err != nil {
		t.Fatalf("Could not close temp file: %v", err)
	}

	// Call LoadNumbers with the temporary file's path
	numbers, err := service.LoadNumbers(tmpfile.Name())
	if err != nil {
		t.Fatalf("LoadNumbers returned an error: %v", err)
	}

	// Define the expected slice
	expected := []int{0, 100, 200, 300}

	// Compare the returned slice with the expected slice
	if len(numbers) != len(expected) {
		t.Fatalf("Expected slice of length %d, got %d", len(expected), len(numbers))
	}
	for i, v := range expected {
		if numbers[i] != v {
			t.Errorf("Expected number at index %d to be %d, got %d", i, v, numbers[i])
		}
	}
}
