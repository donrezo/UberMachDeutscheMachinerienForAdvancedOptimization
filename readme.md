# UberMachDeutscheMachinerienForAdvancedOptimization
UberMachDeutscheMachinerienForAdvancedOptimization is a REST service designed to efficiently locate the index of a given value within a large sorted list of numbers. It utilizes a hexagonal architecture with ports and adapters, ensuring a clean separation of concerns and an emphasis on core business logic.

## Features
* Fast Lookup: Implements a binary search algorithm to quickly find the index of a given number in a preloaded slice of sorted numbers.
* Flexible Conformity: Returns the nearest index within a 10% range if an exact match isn't found.
* Robust Logging: Supports configurable log levels (Info, Debug, Error) to aid in monitoring and troubleshooting.
* Configurable: Allows customization of the service port and log level via a configuration file.
* Test Coverage: Includes unit tests to validate the core components of the application.
* API Endpoint: Exposes a single RESTful endpoint to retrieve the index of a number.

## Getting Started
Prerequisites
Ensure you have the following installed:

* Go (version 1.20 or higher)

## Installation
Clone the repository:

```sh
git clone https://gitlab.com/donrezo/UberMachDeutscheMachinerienForAdvancedOptimization.git
cd UberMachDeutscheMachinerienForAdvancedOptimization
```
## Configuration
Modify the configuration.json file to set the desired server port and log level:

```json
{
"ServerPort": "8080",
"LogLevel": "info"
}
```
## Running the Service
To start the server, run:

```sh
make run
```
The service will start and listen on the configured port.

## Testing the Endpoint
Send a GET request to the /endpoint/{value} route:

```sh
curl http://localhost:<ServerPort>/endpoint/100
```
Replace <ServerPort> with the port configured in configuration.json.

## API Reference


### GET /endpoint/{value}
Retrieves the index of the specified value.

```
URL Params: value=[integer]
Success Response:
Code: 200 OK
Content: { "index": 3, "value": 100 }
Error Response:
Code: 404 Not Found
Content: { "error": "Value not found" }
```

## Testing
Run unit tests with:

```sh
make test
```

## Contributing
Please read CONTRIBUTING.md for details on our code of conduct, and the process for submitting pull requests to us.

## License
This project is licensed under the MIT License - see the LICENSE.md file for details