test:
	 go test ./test/ -v

run:
	go run cmd/main.go

.PHONY: test run