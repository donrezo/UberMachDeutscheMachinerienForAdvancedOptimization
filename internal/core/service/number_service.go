package service

import (
	"UberMachDeutscheMachinerienForAdvancedOptimization/internal/ports"
	"bufio"
	"errors"
	"os"
	"strconv"
)

type NumberService struct {
	numbers []int
	logger  ports.Logger
}

func NewNumberService(numbers []int, logger ports.Logger) *NumberService {
	return &NumberService{numbers: numbers, logger: logger}
}

func (ns *NumberService) FindNumberIndex(value int) (int, int, error) {
	left, right := 0, len(ns.numbers)-1
	for left <= right {
		mid := left + (right-left)/2
		midValue := ns.numbers[mid]
		if midValue == value {
			return mid, midValue, nil
		} else if midValue < value {
			left = mid + 1
		} else {
			right = mid - 1
		}
	}
	return -1, 0, errors.New("number not found")
}

func LoadNumbers(filePath string) ([]int, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var numbers []int
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		number, err := strconv.Atoi(scanner.Text())
		if err != nil {
			continue // handle error or continue based on your requirement
		}
		numbers = append(numbers, number)
	}

	return numbers, scanner.Err()
}
