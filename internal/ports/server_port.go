package ports

type NumberServicePort interface {
	FindNumberIndex(value int) (int, int, error)
}
