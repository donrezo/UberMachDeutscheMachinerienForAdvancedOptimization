package httpHandler

import (
	"UberMachDeutscheMachinerienForAdvancedOptimization/internal/ports"
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

type NumberHandler struct {
	service ports.NumberServicePort
	logger  ports.Logger
}

func NewNumberHandler(service ports.NumberServicePort, logger ports.Logger) *NumberHandler {
	return &NumberHandler{service: service, logger: logger}
}

func (h *NumberHandler) GetNumberIndex(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	value, err := strconv.Atoi(vars["value"])
	if err != nil {
		h.logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	index, number, err := h.service.FindNumberIndex(value)
	if err != nil {
		h.logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	response := map[string]int{"index": index, "value": number}
	parseErr := json.NewEncoder(w).Encode(response)
	if parseErr != nil {
		h.logger.Error(parseErr.Error())
	}
}
