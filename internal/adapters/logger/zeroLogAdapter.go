package logger

import (
	"UberMachDeutscheMachinerienForAdvancedOptimization/internal/ports"
	"github.com/rs/zerolog"
	"io/ioutil"
	"os"
)

type ZerologAdapter struct {
	logger zerolog.Logger
}

func NewZerologAdapter(logLevel string) ports.Logger {
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	level, err := zerolog.ParseLevel(logLevel)
	if err != nil {
		panic(err) // Or handle the error in a way that's appropriate for your app
	}

	// Setting the global log level:
	zerolog.SetGlobalLevel(level)

	// Create a new logger instance:
	logger := zerolog.New(os.Stderr).With().Timestamp().Logger()

	// If the log level is disabled, you can discard all logs like so:
	if level == zerolog.Disabled {
		logger = zerolog.New(ioutil.Discard)
	}

	return &ZerologAdapter{logger: logger}
}

func (z *ZerologAdapter) Error(msg string, keysAndValues ...interface{}) {
	event := z.logger.Error()
	for i := 0; i < len(keysAndValues); i += 2 {
		if i+1 < len(keysAndValues) {
			key, val := keysAndValues[i], keysAndValues[i+1]
			event.Interface(key.(string), val)
		}
	}
	event.Msg(msg)
}

func (z *ZerologAdapter) Debug(msg string, keysAndValues ...interface{}) {
	if zerolog.GlobalLevel() <= zerolog.DebugLevel {
		event := z.logger.Debug()
		for i := 0; i < len(keysAndValues); i += 2 {
			if i+1 < len(keysAndValues) {
				key, val := keysAndValues[i], keysAndValues[i+1]
				event.Interface(key.(string), val)
			}
		}
		event.Msg(msg)
	}
}

func (z *ZerologAdapter) Info(msg string, keysAndValues ...interface{}) {
	if zerolog.GlobalLevel() <= zerolog.InfoLevel {
		event := z.logger.Info()
		for i := 0; i < len(keysAndValues); i += 2 {
			if i+1 < len(keysAndValues) {
				key, val := keysAndValues[i], keysAndValues[i+1]
				event.Interface(key.(string), val)
			}
		}
		event.Msg(msg)
	}
}

func (z *ZerologAdapter) Warn(msg string, keysAndValues ...interface{}) {
	if zerolog.GlobalLevel() <= zerolog.WarnLevel {
		event := z.logger.Warn()
		for i := 0; i < len(keysAndValues); i += 2 {
			if i+1 < len(keysAndValues) {
				key, val := keysAndValues[i], keysAndValues[i+1]
				event.Interface(key.(string), val)
			}
		}
		event.Msg(msg)
	}
}

func (z *ZerologAdapter) Fatal(msg string, keysAndValues ...interface{}) {
	if zerolog.GlobalLevel() <= zerolog.FatalLevel {
		event := z.logger.Fatal()
		for i := 0; i < len(keysAndValues); i += 2 {
			if i+1 < len(keysAndValues) {
				key, val := keysAndValues[i], keysAndValues[i+1]
				event.Interface(key.(string), val)
			}
		}
		event.Msg(msg)
	}
}
