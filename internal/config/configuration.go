package config

import (
	"encoding/json"
	"os"
)

type Configuration struct {
	ServerPort string `json:"ServerPort"`
	LogLevel   string `json:"LogLevel"`
}

func LoadConfiguration(configPath string) (*Configuration, error) {
	var config Configuration
	configFile, err := os.Open(configPath)
	if err != nil {
		return nil, err
	}
	defer configFile.Close()

	jsonParser := json.NewDecoder(configFile)
	err = jsonParser.Decode(&config)
	if err != nil {
		return nil, err
	}

	if config.ServerPort == "" {
		config.ServerPort = "8080"
	}
	if config.LogLevel == "" {
		config.LogLevel = "info"
	}

	return &config, nil
}
