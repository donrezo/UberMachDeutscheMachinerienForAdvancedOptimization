package main

import (
	"UberMachDeutscheMachinerienForAdvancedOptimization/internal/adapters/httpHandler"
	"UberMachDeutscheMachinerienForAdvancedOptimization/internal/adapters/logger"
	"UberMachDeutscheMachinerienForAdvancedOptimization/internal/config"
	"UberMachDeutscheMachinerienForAdvancedOptimization/internal/core/service"
	"github.com/gorilla/mux"
	"net/http"
)

func main() {
	cfg, err := config.LoadConfiguration("./configuration.json")
	if err != nil {
		panic(err)
	}
	logger := logger.NewZerologAdapter(cfg.LogLevel)

	numbers, err := service.LoadNumbers("./input.txt")
	if err != nil {
		logger.Fatal(err.Error())
	}

	numberService := service.NewNumberService(numbers, logger)
	numberHandler := httpHandler.NewNumberHandler(numberService, logger)

	router := mux.NewRouter()
	router.HandleFunc("/endpoint/{value:[0-9]+}", numberHandler.GetNumberIndex).Methods("GET")

	logger.Info("Starting server on port " + cfg.ServerPort)
	logger.Fatal(http.ListenAndServe(":"+cfg.ServerPort, router).Error())
}
